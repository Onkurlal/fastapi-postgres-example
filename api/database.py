from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import os
from dotenv import load_dotenv

# from core.config import settings
load_dotenv()
username = os.environ.get('USERNAME')
password = os.environ.get('PASSWORD')
SQLALCHEMY_DATABASE_URL = f'postgresql://{username}:{password}@localhost/fastapidb'
engine = create_engine(
    SQLALCHEMY_DATABASE_URL
)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()
